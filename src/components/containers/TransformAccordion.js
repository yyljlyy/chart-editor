import PlotlyFold from './PlotlyFold';
import PlotlyPanel from './PlotlyPanel';
import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {connectTransformToTrace} from 'lib';
import {PanelMessage} from './PanelEmpty';

const TransformFold = connectTransformToTrace(PlotlyFold);

class TransformAccordion extends Component {
  render() {
    const {
      fullContainer: {transforms = []},
      localize: _,
      container,
      dataSourceOptions,
    } = this.context;
    const {children} = this.props;

    const transformTypes = [
      {label: _('过滤'), type: 'filter'},
      {label: _('分割'), type: 'groupby'},
      {label: _('聚合'), type: 'aggregate'},
      {label: _('排序'), type: 'sort'},
    ];

    const transformBy =
      container.transforms &&
      container.transforms.map(tr => {
        let foldNameSuffix = '';
        if (tr.groupssrc) {
          const groupssrc =
            dataSourceOptions && dataSourceOptions.find(d => d.value === tr.groupssrc);
          foldNameSuffix = `: ${groupssrc && groupssrc.label ? groupssrc.label : tr.groupssrc}`;
        } else if (tr.targetsrc) {
          const targetsrc =
            dataSourceOptions && dataSourceOptions.find(d => d.value === tr.targetsrc);
          foldNameSuffix = `: ${targetsrc && targetsrc.label ? targetsrc.label : tr.targetsrc}`;
        }
        return foldNameSuffix;
      });

    const filteredTransforms = transforms.filter(({type}) => Boolean(type));
    const content =
      filteredTransforms.length &&
      filteredTransforms.map((tr, i) => (
        <TransformFold
          key={i}
          transformIndex={i}
          name={`${transformTypes.filter(({type}) => type === tr.type)[0].label}${transformBy &&
            transformBy[i]}`}
          canDelete={true}
        >
          {children}
        </TransformFold>
      ));

    // cannot have 2 Split transforms on one trace:
    // https://github.com/plotly/plotly.js/issues/1742
    const addActionOptions =
      container.transforms && container.transforms.some(t => t.type === 'groupby')
        ? transformTypes.filter(t => t.type !== 'groupby')
        : transformTypes;

    const addAction = {
      label: _('数据转换'),
      handler: addActionOptions.map(({label, type}) => {
        return {
          label,
          handler: context => {
            const {fullContainer, updateContainer} = context;
            if (updateContainer) {
              const transformIndex = Array.isArray(fullContainer.transforms)
                ? fullContainer.transforms.length
                : 0;
              const key = `转换[${transformIndex}]`;

              const payload = {type};
              if (type === 'filter') {
                payload.target = [];
                payload.targetsrc = null;
              } else {
                payload.groupssrc = null;
                payload.groups = null;
              }

              if (type === 'groupby') {
                payload.styles = [];
              }

              updateContainer({[key]: payload});
            }
          },
        };
      }),
    };

    return (
      <PlotlyPanel addAction={addAction}>
        {content ? (
          content
        ) : (
          <PanelMessage icon={null}>
            <div style={{textAlign: 'left'}}>
              <p>
                <strong>{_('过滤')}</strong>{' '}
                {_(' 过滤数据')}
              </p>
              <p>
                <strong>{_('分割')}</strong>{' '}
                {_(
                  ' 分割成多个数据并针对不同数据进行设计.'
                )}
              </p>
              <p>
                <strong>{_('聚合')}</strong>{' '}
                {_(
                  ' 使用聚合函数例如"average" 或 "minimum"对你的数据汇总.'
                )}
              </p>
              <p>
                <strong>{_('排序')}</strong>{' '}
                {_(
                  ' 对你的数据进行排序.'
                )}
              </p>
            </div>
            <p>{_('点击上面的 + 按钮来添加一个数据转换.')}</p>
          </PanelMessage>
        )}
      </PlotlyPanel>
    );
  }
}

TransformAccordion.contextTypes = {
  fullContainer: PropTypes.object,
  localize: PropTypes.func,
  container: PropTypes.object,
  dataSourceOptions: PropTypes.array,
};

TransformAccordion.propTypes = {
  children: PropTypes.node,
};

export default TransformAccordion;
