import PlotlyFold from './PlotlyFold';
import {LayoutPanel} from './derived';
import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {connectImageToLayout} from 'lib';
import {PanelMessage} from './PanelEmpty';

const ImageFold = connectImageToLayout(PlotlyFold);

class ImageAccordion extends Component {
  render() {
    const {
      layout: {images = []},
      localize: _,
    } = this.context;
    const {canAdd, children} = this.props;

    const content =
      images.length &&
      images.map((img, i) => (
        <ImageFold key={i} imageIndex={i} name={`${_('Image')} ${i + 1}`} canDelete={canAdd}>
          {children}
        </ImageFold>
      ));

    const addAction = {
      label: _('添加图片'),
      handler: ({layout, updateContainer}) => {
        let imageIndex;
        if (Array.isArray(layout.images)) {
          imageIndex = layout.images.length;
        } else {
          imageIndex = 0;
        }

        const key = `images[${imageIndex}]`;
        const value = {
          sizex: 0.1,
          sizey: 0.1,
          x: 0.5,
          y: 0.5,
        };

        if (updateContainer) {
          updateContainer({[key]: value});
        }
      },
    };

    return (
      <LayoutPanel addAction={canAdd ? addAction : null}>
        {content ? (
          content
        ) : (
          <PanelMessage heading={_('添加标识、水印等.')}>
            <p>
              {_(
                '插入图像在您的图表中.'
              )}
            </p>
            <p>{_('点击上面的 + 按钮添加一个图像.')}</p>
          </PanelMessage>
        )}
      </LayoutPanel>
    );
  }
}

ImageAccordion.contextTypes = {
  layout: PropTypes.object,
  localize: PropTypes.func,
};

ImageAccordion.propTypes = {
  children: PropTypes.node,
  canAdd: PropTypes.bool,
};

export default ImageAccordion;
