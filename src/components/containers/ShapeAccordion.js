import PlotlyFold from './PlotlyFold';
import {LayoutPanel} from './derived';
import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {connectShapeToLayout} from 'lib';
import {COLORS} from 'lib/constants';
import {PanelMessage} from './PanelEmpty';

const ShapeFold = connectShapeToLayout(PlotlyFold);

class ShapeAccordion extends Component {
  render() {
    const {
      layout: {shapes = []},
      localize: _,
    } = this.context;
    const {canAdd, children} = this.props;

    const content =
      shapes.length &&
      shapes.map((shp, i) => (
        <ShapeFold key={i} shapeIndex={i} name={`${_('Shape')} ${i + 1}`} canDelete={canAdd}>
          {children}
        </ShapeFold>
      ));

    const addAction = {
      label: _('tianjin形状'),
      handler: ({layout, updateContainer}) => {
        let shapeIndex;
        if (Array.isArray(layout.shapes)) {
          shapeIndex = layout.shapes.length;
        } else {
          shapeIndex = 0;
        }

        const key = `shapes[${shapeIndex}]`;
        const value = {
          line: {color: COLORS.charcoal},
          fillcolor: COLORS.middleGray,
          opacity: 0.3,
        };

        if (updateContainer) {
          updateContainer({[key]: value});
        }
      },
    };

    return (
      <LayoutPanel addAction={canAdd ? addAction : null}>
        {content ? (
          content
        ) : (
          <PanelMessage heading={_('直线、矩形和椭圆.')}>
            <p>
              {_(
                '向图形添加形状，以突出显示点或时间段、阈值或感兴趣的区域.'
              )}
            </p>
            <p>{_('点击上面的 + 按钮来添加一个形状.')}</p>
          </PanelMessage>
        )}
      </LayoutPanel>
    );
  }
}

ShapeAccordion.contextTypes = {
  layout: PropTypes.object,
  localize: PropTypes.func,
};

ShapeAccordion.propTypes = {
  children: PropTypes.node,
  canAdd: PropTypes.bool,
};

export default ShapeAccordion;
