import React from 'react';
import PropTypes from 'prop-types';
import {
  ColorPicker,
  FontSelector,
  Numeric,
  PlotlySection,
  UpdateMenuAccordion,
  UpdateMenuButtons,
  VisibilitySelect,
  Radio,
} from '../components';

const StyleUpdateMenusPanel = (props, {localize: _}) => (
  <UpdateMenuAccordion>
    <VisibilitySelect
      attr="visible"
      options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      showOn={true}
    >
      <PlotlySection name={_('按钮标签')}>
        <UpdateMenuButtons attr="buttons" />
      </PlotlySection>
      <PlotlySection name={_('背景')}>
        <ColorPicker label={_('颜色')} attr="bgcolor" />
      </PlotlySection>
      <PlotlySection name={_('字体')}>
        <FontSelector label={_('字体')} attr="font.family" />
        <Numeric label={_('大小')} attr="font.size" />
        <ColorPicker label={_('颜色')} attr="font.color" />
      </PlotlySection>
      <PlotlySection name={_('边界')}>
        <Numeric label={_('宽度')} attr="borderwidth" />
        <ColorPicker label={_('颜色')} attr="bordercolor" />
      </PlotlySection>

      <PlotlySection name={_('水平定位')} attr={'x'}>
        <Numeric label={_('位置')} attr={'x'} showSlider step={0.02} />
        <Radio
          label={_('锚')}
          attr={'xanchor'}
          options={[
            {label: _('左'), value: 'left'},
            {label: _('居中'), value: 'center'},
            {label: _('右'), value: 'right'},
          ]}
        />
      </PlotlySection>
      <PlotlySection name={_('垂直定位')} attr={'y'}>
        <Numeric label={_('位置')} attr={'y'} showSlider step={0.02} />
        <Radio
          label={_('锚')}
          attr={'yanchor'}
          options={[
            {label: _('顶部'), value: 'top'},
            {label: _('中部'), value: 'middle'},
            {label: _('底部'), value: 'bottom'},
          ]}
        />
      </PlotlySection>

      <PlotlySection name={_('间隔')}>
        <Numeric label={_('顶部')} attr="pad.t" units="px" />
        <Numeric label={_('底部')} attr="pad.b" units="px" />
        <Numeric label={_('左')} attr="pad.l" units="px" />
        <Numeric label={_('右')} attr="pad.r" units="px" />
      </PlotlySection>
    </VisibilitySelect>
  </UpdateMenuAccordion>
);

StyleUpdateMenusPanel.contextTypes = {
  localize: PropTypes.func,
};

export default StyleUpdateMenusPanel;
