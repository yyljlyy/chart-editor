import React from 'react';
import PropTypes from 'prop-types';
import {
  AnnotationArrowRef,
  AnnotationRef,
  AnnotationAccordion,
  ArrowSelector,
  ColorPicker,
  FontSelector,
  Numeric,
  Dropdown,
  PositioningNumeric,
  Radio,
  TextEditor,
  PlotlySection,
} from '../components';

const StyleNotesPanel = (props, {localize: _}) => (
  <AnnotationAccordion canAdd>
    <PlotlySection name={_('注释文本')} attr="text">
      <TextEditor attr="text" />
      <FontSelector label={_('字体')} attr="font.family" />
      <Numeric label={_('字体大小')} attr="font.size" units="px" />
      <ColorPicker label={_('字体颜色')} attr="font.color" />
      <Numeric label={_('角度')} attr="textangle" units="°" />
    </PlotlySection>
    <PlotlySection name={_('箭头')}>
      <Radio
        attr="showarrow"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
      <Numeric label={_('线宽')} attr="arrowwidth" units="px" />
      <ColorPicker label={_('颜色')} attr="arrowcolor" />
      <ArrowSelector label={_('箭头')} attr="arrowhead" />
      <Numeric label={_('刻度')} step={0.1} attr="arrowsize" units="px" />
      <AnnotationArrowRef label={_('X 偏移量')} attr="axref" />
      <AnnotationArrowRef label={_('Y 偏移量')} attr="ayref" />
      <Numeric label={_('X 向量')} attr="ax" />
      <Numeric label={_('Y 向量')} attr="ay" />
    </PlotlySection>
    <PlotlySection name={_('水平定位')}>
      <Dropdown
        label={_('锚点')}
        clearable={false}
        attr="xanchor"
        options={[
          {label: _('自动'), value: 'auto'},
          {label: _('左'), value: 'left'},
          {label: _('居中'), value: 'center'},
          {label: _('右'), value: 'right'},
        ]}
      />
      <PositioningNumeric label={_('位置')} attr="x" />
      <AnnotationRef label={_('Relative To')} attr="xref" />
    </PlotlySection>
    <PlotlySection name={_('垂直定位')}>
      <Dropdown
        label={_('锚点')}
        clearable={false}
        attr="yanchor"
        options={[
          {label: _('自动'), value: 'auto'},
          {label: _('顶部'), value: 'top'},
          {label: _('中部'), value: 'middle'},
          {label: _('底部'), value: 'bottom'},
        ]}
      />
      <PositioningNumeric label={_('位置')} attr="y" />
      <AnnotationRef label={_('Relative To')} attr="yref" />
    </PlotlySection>
  </AnnotationAccordion>
);

StyleNotesPanel.contextTypes = {
  localize: PropTypes.func,
};

export default StyleNotesPanel;
