import React from 'react';
import PropTypes from 'prop-types';
import {
  ColorPicker,
  ColorwayPicker,
  Dropdown,
  FontSelector,
  PlotlyFold,
  Numeric,
  TextEditor,
  PlotlySection,
  LayoutPanel,
  VisibilitySelect,
  HovermodeDropdown,
  Flaglist,
  Radio,
} from '../components';
import {HoverColor} from '../components/fields/derived';

const StyleLayoutPanel = (props, {localize: _}) => (
  <LayoutPanel>
    <PlotlyFold name={_('默认值')}>
      <ColorPicker label={_('Plot Background')} attr="plot_bgcolor" />
      <ColorPicker label={_('Margin color')} attr="paper_bgcolor" />
      <ColorwayPicker label={_('底色')} attr="colorway" />
      <FontSelector label={_('字体')} attr="font.family" clearable={false} />
      <Numeric label={_('字体大小')} attr="font.size" units="px" />
      <ColorPicker label={_('字体颜色')} attr="font.color" />
      <Dropdown
        label={_('数字格式')}
        attr="separators"
        options={[
          {label: _('1,234.56'), value: '.,'},
          {label: _('1 234.56'), value: ', '},
          {label: _('1 234,56'), value: ', '},
          {label: _('1.234,56'), value: ',.'},
        ]}
        clearable={false}
      />
    </PlotlyFold>

    <PlotlyFold name={_('标题')}>
      <PlotlySection name={_('标题')} attr="title">
        <TextEditor attr="title" />
        <FontSelector label={_('字体')} attr="titlefont.family" clearable={false} />
        <Numeric label={_('字体大小')} attr="titlefont.size" units="px" />
        <ColorPicker label={_('字体颜色')} attr="titlefont.color" />
      </PlotlySection>
    </PlotlyFold>

    <PlotlyFold name={_('Modebar')}>
      <Radio
        label={_('方向')}
        attr="modebar.orientation"
        options={[{label: _('水平'), value: 'h'}, {label: _('垂直'), value: 'v'}]}
      />
      <ColorPicker label={_('图标颜色')} attr="modebar.color" />
      <ColorPicker label={_('移动图标颜色')} attr="modebar.activecolor" />
      <ColorPicker label={_('背景颜色')} attr="modebar.bgcolor" />
    </PlotlyFold>
    <PlotlyFold name={_('布局')}>
      <VisibilitySelect
        attr="autosize"
        label={_('大小')}
        options={[{label: _('自动'), value: true}, {label: _('Custom'), value: false}]}
        showOn={false}
        defaultOpt={true}
      >
        <Numeric label={_('固定宽度')} attr="width" units="px" />
        <Numeric label={_('固定高度')} attr="height" units="px" />
      </VisibilitySelect>
      <Numeric label={_('顶部')} attr="margin.t" units="px" />
      <Numeric label={_('底部')} attr="margin.b" units="px" />
      <Numeric label={_('左')} attr="margin.l" units="px" />
      <Numeric label={_('右')} attr="margin.r" units="px" />
      <Numeric label={_('Padding')} attr="margin.pad" units="px" />
    </PlotlyFold>
    <PlotlyFold name={_('Interactions')}>
      <PlotlySection name={_('拖')} attr="dragmode">
        <Dropdown
          label={_('模式')}
          attr="dragmode"
          options={[
            {label: _('Zoom'), value: 'zoom'},
            {label: _('Select'), value: 'select'},
            {label: _('Pan'), value: 'pan'},
            {label: _('Lasso'), value: 'lasso'},
            {label: _('Orbit'), value: 'orbit'},
            {label: _('Turntable'), value: 'turntable'},
          ]}
          clearable={false}
        />
        <Dropdown
          label={_('选择方向')}
          attr="selectdirection"
          options={[
            {label: _('任意'), value: 'any'},
            {label: _('水平'), value: 'h'},
            {label: _('垂直'), value: 'v'},
            {label: _('对角'), value: 'd'},
          ]}
          clearable={false}
        />
      </PlotlySection>
      <PlotlySection name={_('单击')} attr="clickmode">
        <Flaglist
          label={_('模式')}
          attr="clickmode"
          options={[
            {label: _('单击动作'), value: 'event'},
            {label: _('选择数据点'), value: 'select'},
          ]}
        />
      </PlotlySection>
      <PlotlySection name={_('悬停')}>
        <HovermodeDropdown label={_('模式')} attr="hovermode">
          <HoverColor
            label={_('背景色')}
            attr="hoverlabel.bgcolor"
            defaultColor="#FFF"
            handleEmpty
          />
          <HoverColor
            label={_('边框颜色')}
            attr="hoverlabel.bordercolor"
            defaultColor="#000"
            handleEmpty
          />
          <FontSelector label={_('字体')} attr="hoverlabel.font.family" clearable />
          <Numeric label={_('字体大小')} attr="hoverlabel.font.size" />
          <HoverColor
            label={_('字体颜色')}
            attr="hoverlabel.font.color"
            defaultColor="#000"
            handleEmpty
          />
        </HovermodeDropdown>
      </PlotlySection>
    </PlotlyFold>
  </LayoutPanel>
);

StyleLayoutPanel.contextTypes = {
  localize: PropTypes.func,
};

export default StyleLayoutPanel;
