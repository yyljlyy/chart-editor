import React from 'react';
import PropTypes from 'prop-types';
import {
  ColorPicker,
  FontSelector,
  PlotlyFold,
  Numeric,
  Radio,
  PlotlySection,
  Dropdown,
  TraceRequiredPanel,
} from '../components';

const StyleLegendPanel = (props, {localize: _}) => (
  <TraceRequiredPanel>
    <PlotlyFold name={_('图例')}>
      <Radio
        attr="showlegend"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
      <PlotlySection name={_('文本')}>
        <FontSelector label={_('字体')} attr="legend.font.family" />
        <Numeric label={_('大小')} attr="legend.font.size" units="px" />
        <ColorPicker label={_('颜色')} attr="legend.font.color" />
      </PlotlySection>
      <PlotlySection name={_('图例框')}>
        <Numeric label={_('边界宽度')} attr="legend.borderwidth" units="px" />
        <ColorPicker label={_('边界颜色')} attr="legend.bordercolor" />
        <ColorPicker label={_('背景色')} attr="legend.bgcolor" />
      </PlotlySection>
      <PlotlySection name={_('水平定位')}>
        <Dropdown
          label={_('锚点')}
          clearable={false}
          attr="legend.xanchor"
          options={[
            {label: _('自动'), value: 'auto'},
            {label: _('左'), value: 'left'},
            {label: _('居中'), value: 'center'},
            {label: _('右'), value: 'right'},
          ]}
        />
        <Numeric label={_('位置')} showSlider step={0.02} attr="legend.x" />
      </PlotlySection>
      <PlotlySection name={_(' 垂直定位')}>
        <Dropdown
          label={_('锚点')}
          clearable={false}
          attr="legend.yanchor"
          options={[
            {label: _('自动'), value: 'auto'},
            {label: _('顶部'), value: 'top'},
            {label: _('中部'), value: 'middle'},
            {label: _('底部'), value: 'bottom'},
          ]}
        />
        <Numeric label={_('位置')} showSlider step={0.02} attr="legend.y" />
      </PlotlySection>
      <PlotlySection name={_('方向')}>
        <Radio
          attr="legend.orientation"
          options={[{label: _('垂直'), value: 'v'}, {label: _('水平'), value: 'h'}]}
        />
      </PlotlySection>
      <PlotlySection name={_('迹序')}>
        <Dropdown
          attr="legend.traceorder"
          options={[
            {label: _('正常'), value: 'normal'},
            {label: _('反向'), value: 'reversed'},
            {label: _('分组'), value: 'grouped'},
            {label: _('反向分组'), value: 'reversed+grouped'},
          ]}
        />
        <Numeric label={_('组间距')} attr="legend.tracegroupgap" units="px" />
      </PlotlySection>
    </PlotlyFold>
  </TraceRequiredPanel>
);

StyleLegendPanel.contextTypes = {
  localize: PropTypes.func,
};

export default StyleLegendPanel;
