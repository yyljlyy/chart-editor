import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  AxesRange,
  DTicks,
  NTicks,
  ColorPicker,
  Dropdown,
  FontSelector,
  Numeric,
  NumericFraction,
  Radio,
  TextEditor,
  PlotlySection,
  LayoutPanel,
  AxesFold,
  AxisSide,
  RangesliderVisible,
  RangeSelectorAccordion,
  VisibilitySelect,
  DropdownCustom,
  TickFormat,
} from '../components';

class StyleAxesPanel extends Component {
  render() {
    const {localize: _} = this.context;
    return (
      <LayoutPanel>
        <AxesFold
          name={_('标题')}
          axisFilter={axis => !(axis._name.includes('angular') || axis._subplot.includes('geo'))}
        >
          <TextEditor attr="title" />
          <FontSelector label={_('字体')} attr="titlefont.family" />
          <Numeric label={_('字体大小')} attr="titlefont.size" units="px" />
          <ColorPicker label={_('字体颜色')} attr="titlefont.color" />
        </AxesFold>

        <AxesFold name={_('范围')}>
          <PlotlySection name={_('范围')} attr="autorange">
            <Dropdown
              attr="type"
              label={_('类型')}
              clearable={false}
              options={[
                {label: _('线性'), value: 'linear'},
                {label: _('Log'), value: 'log'},
                {label: _('日期'), value: 'date'},
                {label: _('类别'), value: 'category'},
              ]}
            />
            <Radio
              attr="autorange"
              label={_('范围')}
              options={[{label: _('自动'), value: true}, {label: _('自定义'), value: false}]}
            />
            <AxesRange label={_('最小值')} attr="range[0]" />
            <AxesRange label={_('最大值')} attr="range[1]" />
            <Numeric label={_('最小值')} attr="min" />
          </PlotlySection>
          <PlotlySection name={_('缩放交互')} attr="fixedrange">
            <Radio
              attr="fixedrange"
              options={[{label: _('可行'), value: false}, {label: _('不可行'), value: true}]}
            />
          </PlotlySection>
          <Dropdown
            label={_('方向')}
            attr="direction"
            options={[
              {label: _('顺时针方向'), value: 'clockwise'},
              {label: _('逆时针方向'), value: 'counterclockwise'},
            ]}
            clearable={false}
          />
        </AxesFold>

        <AxesFold name={_('线')}>
          <PlotlySection name={_('轴线')} attr="showline">
            <VisibilitySelect
              attr="showline"
              options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
              showOn={true}
              defaultOpt={true}
            >
              <Numeric label={_('厚度')} attr="linewidth" units="px" />
              <ColorPicker label={_('颜色')} attr="linecolor" />

              <AxisSide label={_('位置')} attr="side" />
              <Radio
                label={_('镜像轴')}
                attr="mirror"
                options={[{label: _('打开'), value: 'ticks'}, {label: _('关闭'), value: false}]}
              />
            </VisibilitySelect>
          </PlotlySection>
          <PlotlySection name={_('网格线')} attr="showgrid">
            <VisibilitySelect
              attr="showgrid"
              options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
              showOn={true}
              defaultOpt={true}
            >
              <Numeric label={_('厚度')} attr="gridwidth" units="px" />
              <ColorPicker label={_('颜色')} attr="gridcolor" />

              <Radio
                label={_('网格间距')}
                attr="tickmode"
                options={[{label: _('自动'), value: 'auto'}, {label: _('自定义'), value: 'linear'}]}
              />

              <DTicks label={_('步长偏移量')} attr="tick0" />
              <DTicks label={_('步长大小')} attr="dtick" />
              <NTicks label={_('最大行数')} attr="nticks" />
            </VisibilitySelect>
          </PlotlySection>
          <PlotlySection name={_('零线')} attr="zeroline">
            <Radio
              attr="zeroline"
              options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
            />
            <Numeric label={_('厚度')} attr="zerolinewidth" units="px" />
            <ColorPicker label={_('颜色')} attr="zerolinecolor" />
          </PlotlySection>

          <PlotlySection name={_('轴背景')} attr="showbackground">
            <Radio
              attr="showbackground"
              options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
            />
            <ColorPicker label={_('颜色')} attr="backgroundcolor" />
          </PlotlySection>
        </AxesFold>

        <AxesFold name={_('刻度标记')} axisFilter={axis => !axis._subplot.includes('geo')}>
          <PlotlySection name={_('刻度标记')} attr="showticklabels">
            <VisibilitySelect
              attr="showticklabels"
              options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
              showOn={true}
              defaultOpt={true}
            >
              <AxisSide label={_('位置')} attr="side" />
              <Radio
                label={_('Auto margins')}
                attr="automargin"
                options={[{label: _('真'), value: true}, {label: _('假'), value: false}]}
              />
              <FontSelector label={_('字体')} attr="tickfont.family" />
              <Numeric label={_('字体大小')} attr="tickfont.size" units="px" />
              <ColorPicker label={_('字体颜色')} attr="tickfont.color" />
              <Dropdown
                label={_('角度')}
                attr="tickangle"
                clearable={false}
                options={[
                  {label: _('自动'), value: 'auto'},
                  {label: _('45'), value: 45},
                  {label: _('90'), value: 90},
                  {label: _('135'), value: 135},
                  {label: _('180'), value: 180},
                ]}
              />

              <TickFormat
                label={_('标签格式')}
                attr="tickformat"
                dafaultOpt=""
                clearable={false}
              />
              <Radio
                label={_('Separate Thousands')}
                attr="separatethousands"
                options={[{label: _('真'), value: true}, {label: _('假'), value: false}]}
              />
              <Dropdown
                label={_('指数')}
                attr="exponentformat"
                clearable={false}
                options={[
                  {label: _('空'), value: '000'},
                  {label: _('e+6'), value: 'e'},
                  {label: _('E+6'), value: 'E'},
                  {label: _('x10^6'), value: 'power'},
                  {label: _('k/M/G'), value: 'SI'},
                  {label: _('k/M/B'), value: 'B'},
                ]}
              />
              <Dropdown
                label={_('显示指数')}
                attr="showexponent"
                clearable={false}
                options={[
                  {label: _('全部'), value: 'all'},
                  {label: _('第一个'), value: 'first'},
                  {label: _('最后一个'), value: 'last'},
                  {label: _('空'), value: 'none'},
                ]}
              />

              <DropdownCustom
                label={_('前缀')}
                attr="tickprefix"
                options={[
                  {label: _('空'), value: ''},
                  {label: _('x'), value: 'x'},
                  {label: _('$'), value: '$'},
                  {label: _('#'), value: '#'},
                  {label: _('@'), value: '@'},
                  {label: _('自定义'), value: 'custom'},
                ]}
                customOpt="custom"
                dafaultOpt=""
                clearable={false}
              />
              <Dropdown
                label={_('显示前缀')}
                attr="showtickprefix"
                options={[
                  {label: _('任一标签'), value: 'all'},
                  {label: _('第一个标签'), value: 'first'},
                  {label: _('最后一个标签'), value: 'last'},
                  {label: _('空'), value: 'none'},
                ]}
              />
              <DropdownCustom
                label={_('后缀')}
                attr="ticksuffix"
                options={[
                  {label: _('空'), value: ''},
                  {label: _('C'), value: 'C'},
                  {label: _('%'), value: '%'},
                  {label: _('^'), value: '^'},
                  {label: _('自定义'), value: 'custom'},
                ]}
                customOpt="custom"
                dafaultOpt=""
                clearable={false}
              />
              <Dropdown
                label={_('显示后缀')}
                attr="showticksuffix"
                options={[
                  {label: _('任一标签'), value: 'all'},
                  {label: _('第一个标签'), value: 'first'},
                  {label: _('最后一个标签'), value: 'last'},
                  {label: _('空'), value: 'none'},
                ]}
              />

              <Radio
                label={_('Tick Spacing')}
                attr="tickmode"
                options={[{label: _('Auto'), value: 'auto'}, {label: _('Custom'), value: 'linear'}]}
              />

              <DTicks label={_('步长偏移量')} attr="tick0" />
              <DTicks label={_('步长大小')} attr="dtick" />
              <NTicks label={_('最大标签数')} attr="nticks" />
            </VisibilitySelect>
          </PlotlySection>
        </AxesFold>
        <AxesFold name={_('打勾标记')} axisFilter={axis => !axis._subplot.includes('geo')}>
          <PlotlySection name={_('打勾标记')} attr="ticks">
            <VisibilitySelect
              attr="ticks"
              options={[
                {label: _('内部'), value: 'inside'},
                {label: _('外部'), value: 'outside'},
                {label: _('隐藏'), value: ''},
              ]}
              showOn={['inside', 'outside']}
              defaultOpt={'Outside'}
            >
              <AxisSide label={_('位置')} attr="side" />
              <Numeric label={_('长度')} attr="ticklen" units="px" />
              <Numeric label={_('宽度')} attr="tickwidth" units="px" />
              <ColorPicker label={_('颜色')} attr="tickcolor" />
              <Radio
                label={_('打勾间距')}
                attr="tickmode"
                options={[{label: _('自动'), value: 'auto'}, {label: _('自定义'), value: 'linear'}]}
              />

              <DTicks label={_('步长偏移量')} attr="tick0" />
              <DTicks label={_('步长大小')} attr="dtick" />
              <NTicks label={_('最大标记数')} attr="nticks" />
            </VisibilitySelect>
          </PlotlySection>
        </AxesFold>

        <AxesFold name={_('滑动范围')} axisFilter={axis => axis._subplot.includes('xaxis')}>
          <RangesliderVisible
            attr="rangeslider.visible"
            options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
          />
          <NumericFraction label={_('高度')} attr="rangeslider.thickness" />
          <ColorPicker label={_('背景色')} attr="rangeslider.bgcolor" />
          <Numeric label={_('边框宽度')} attr="rangeslider.borderwidth" units="px" />
          <ColorPicker label={_('边框颜色')} attr="rangeslider.bordercolor" />
        </AxesFold>

        <AxesFold
          name={_('时间刻度按钮')}
          axisFilter={axis => axis._subplot.includes('xaxis') && axis.type === 'date'}
        >
          <Radio
            attr="rangeselector.visible"
            options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
          />

          <RangeSelectorAccordion>
            <TextEditor attr="label" label={_('Label')} show />
            <Numeric label={_('计数')} attr="count" />
            <Dropdown
              label={_('步长')}
              attr="step"
              clearable={false}
              options={[
                {label: _('年'), value: 'year'},
                {label: _('月'), value: 'month'},
                {label: _('日'), value: 'day'},
                {label: _('时'), value: 'hour'},
                {label: _('分'), value: 'minute'},
                {label: _('秒'), value: 'second'},
                {label: _('全部'), value: 'all'},
              ]}
            />
            <Dropdown
              label={_('步长模式')}
              attr="stepmode"
              clearable={false}
              options={[
                {label: _('迄今为止'), value: 'todate'},
                {label: _('向后'), value: 'backward'},
              ]}
            />
          </RangeSelectorAccordion>
          <PlotlySection name={_('文本')}>
            <FontSelector label={_('字体')} attr="rangeselector.font.family" />
            <Numeric label={_('字体大小')} attr="rangeselector.font.size" units="px" />
            <ColorPicker label={_('字体颜色')} attr="rangeselector.font.color" />
          </PlotlySection>
          <PlotlySection name={_('类型')}>
            <ColorPicker label={_('背景色')} attr="rangeselector.bgcolor" />
            <ColorPicker label={_('激活颜色')} attr="rangeselector.activecolor" />
            <Numeric label={_('边框宽度')} attr="rangeselector.borderwidth" units="px" />
            <ColorPicker label={_('边框颜色')} attr="rangeselector.bordercolor" />
          </PlotlySection>
          <PlotlySection name={_('水平定位')}>
            <Dropdown
              label={_('锚点')}
              clearable={false}
              attr="rangeselector.xanchor"
              options={[
                {label: _('自动'), value: 'auto'},
                {label: _('左'), value: 'left'},
                {label: _('居中'), value: 'center'},
                {label: _('右'), value: 'right'},
              ]}
            />
            <Numeric label={_('位置')} step={0.02} attr="rangeselector.x" />
          </PlotlySection>
          <PlotlySection name={_('垂直定位')}>
            <Dropdown
              label={_('锚点')}
              clearable={false}
              attr="rangeselector.yanchor"
              options={[
                {label: _('自动'), value: 'auto'},
                {label: _('顶部'), value: 'top'},
                {label: _('中部'), value: 'middle'},
                {label: _('底部'), value: 'bottom'},
              ]}
            />
            <Numeric label={_('位置')} step={0.02} attr="rangeselector.y" />
          </PlotlySection>
        </AxesFold>

        <AxesFold
          name={_('穗线')}
          axisFilter={axis =>
            !(
              axis._subplot.includes('ternary') ||
              axis._subplot.includes('polar') ||
              axis._subplot.includes('geo')
            )
          }
        >
          <Radio
            attr="showspikes"
            options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
          />
          <Radio
            attr="spikesides"
            label={_('显示侧')}
            options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
          />

          <Numeric label={_('厚度')} attr="spikethickness" units="px" />
          <ColorPicker label={_('颜色')} attr="spikecolor" />
        </AxesFold>
      </LayoutPanel>
    );
  }
}

StyleAxesPanel.contextTypes = {
  fullLayout: PropTypes.object,
  localize: PropTypes.func,
};

export default StyleAxesPanel;
