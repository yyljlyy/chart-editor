import React from 'react';
import PropTypes from 'prop-types';
import {
  ImageAccordion,
  Radio,
  Dropzone,
  PositioningNumeric,
  PlotlySection,
  PositioningRef,
  Dropdown,
} from '../components';

const StyleImagesPanel = (props, {localize: _}) => (
  <ImageAccordion canAdd>
    <Radio
      attr="visible"
      options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
    />

    <Dropzone attr="source" fileType={_('image')} show />

    <Dropdown
      label={_('宽高比o')}
      attr="sizing"
      options={[
        {label: _('包含'), value: 'contain'},
        {label: _('F填充ill'), value: 'fill'},
        {label: _('拉伸'), value: 'stretch'},
      ]}
      clearable={false}
    />
    <Radio
      label={_('与网格关系d')}
      attr="layer"
      options={[{label: _('位于底层'), value: 'below'}, {label: _('位于顶层'), value: 'above'}]}
    />
    <PositioningNumeric attr="sizex" label={_('Width')} />
    <PositioningNumeric attr="sizey" label={_('Height')} />
    <PlotlySection name={_('水平定位')}>
      <Dropdown
        label={_('锚点')}
        clearable={false}
        attr="xanchor"
        options={[
          {label: _('左'), value: 'left'},
          {label: _('居中'), value: 'center'},
          {label: _('右'), value: 'right'},
        ]}
      />
      <PositioningNumeric label={_('位置')} attr="x" />
      <PositioningRef label={_('Relative To')} attr="xref" />
    </PlotlySection>

    <PlotlySection name={_('垂直定位')}>
      <Dropdown
        label={_('锚点')}
        clearable={false}
        attr="yanchor"
        options={[
          {label: _('顶部'), value: 'top'},
          {label: _('中部'), value: 'middle'},
          {label: _('底部'), value: 'bottom'},
        ]}
      />
      <PositioningNumeric label={_('位置')} attr="y" />
      <PositioningRef label={_('Relative To')} attr="yref" />
    </PlotlySection>
  </ImageAccordion>
);

StyleImagesPanel.contextTypes = {
  localize: PropTypes.func,
};

export default StyleImagesPanel;
