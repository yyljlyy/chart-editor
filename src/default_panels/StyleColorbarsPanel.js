import React from 'react';
import PropTypes from 'prop-types';

import {
  Radio,
  TextEditor,
  TraceAccordion,
  Numeric,
  PlotlyFold,
  PlotlyPanel,
  PlotlySection,
  Dropdown,
  DropdownCustom,
  FontSelector,
  ColorPicker,
  VisibilitySelect,
} from '../components';

export const traceHasColorbar = (trace, fullTrace) =>
  (fullTrace.marker && fullTrace.marker.showscale !== undefined) || // eslint-disable-line no-undefined
  fullTrace.showscale !== undefined; // eslint-disable-line no-undefined

const StyleColorBarsPanel = (props, {localize: _}) => {
  return (
    <TraceAccordion traceFilterCondition={traceHasColorbar}>
      {['', 'marker.'].map(prefix => {
        return (
          <VisibilitySelect
            attr={prefix + 'showscale'}
            key={'x' + prefix}
            options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
            showOn={true}
          >
            <PlotlyPanel key={prefix + ' panel'}>
              <PlotlyFold name={_('标题')}>
                <TextEditor attr={prefix + 'colorbar.title'} />

                <Dropdown
                  label={_('位置')}
                  attr={prefix + 'colorbar.titleside'}
                  options={[
                    {label: _('顶部'), value: 'top'},
                    {label: _('右'), value: 'right'},
                    {label: _('底部'), value: 'bottom'},
                  ]}
                />
                <FontSelector label={_('字体')} attr={prefix + 'colorbar.titlefont.family'} />
                <Numeric
                  label={_('字体大小')}
                  attr={prefix + 'colorbar.titlefont.size'}
                  units="px"
                />
                <ColorPicker label={_('字体颜色')} attr={prefix + 'colorbar.titlefont.color'} />
              </PlotlyFold>
              <PlotlyFold name={_('大小与定位')}>
                <PlotlySection name={_('大小')} attr={prefix + 'colorbar.len'}>
                  <Numeric label={_('高度')} attr={prefix + 'colorbar.len'} />

                  <Radio
                    attr={prefix + 'colorbar.lenmode'}
                    options={[
                      {label: _('制图分数'), value: 'fraction'},
                      {label: _('像素'), value: 'pixels'},
                    ]}
                  />

                  <Numeric label={_('宽度')} attr={prefix + 'colorbar.thickness'} />

                  <Radio
                    attr={prefix + 'colorbar.thicknessmode'}
                    options={[
                      {label: _('Fraction of Plot'), value: 'fraction'},
                      {label: _('像素'), value: 'pixels'},
                    ]}
                  />
                </PlotlySection>
                <PlotlySection name={_('Horizontal Positioning')} attr={prefix + 'colorbar.x'}>
                  <Numeric
                    label={_('位置')}
                    attr={prefix + 'colorbar.x'}
                    showSlider
                    step={0.02}
                  />
                  <Dropdown
                    label={_('锚')}
                    attr={prefix + 'colorbar.xanchor'}
                    options={[
                      {label: _('左'), value: 'left'},
                      {label: _('居中'), value: 'center'},
                      {label: _('右'), value: 'right'},
                    ]}
                  />
                </PlotlySection>
                <PlotlySection name={_('垂直定位')} attr={prefix + 'colorbar.y'}>
                  <Numeric
                    label={_('位置')}
                    attr={prefix + 'colorbar.y'}
                    showSlider
                    step={0.02}
                  />
                  <Dropdown
                    label={_('锚')}
                    attr={prefix + 'colorbar.yanchor'}
                    options={[
                      {label: _('顶部'), value: 'top'},
                      {label: _('中部'), value: 'middle'},
                      {label: _('底部'), value: 'bottom'},
                    ]}
                  />
                </PlotlySection>
                <PlotlySection name={_('间隔')} attr={prefix + 'colorbar.xpad'}>
                  <Numeric label={_('垂直')} attr={prefix + 'colorbar.ypad'} units="px" />
                  <Numeric label={_('水平')} attr={prefix + 'colorbar.xpad'} units="px" />
                </PlotlySection>
              </PlotlyFold>
              <PlotlyFold name={_('标签')}>
                <VisibilitySelect
                  attr={prefix + 'colorbar.showticklabels'}
                  options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
                  showOn={true}
                  defaultOpt={true}
                >
                  <FontSelector label={_('字体')} attr={prefix + 'colorbar.tickfont.family'} />
                  <Numeric
                    label={_('字体大小')}
                    attr={prefix + 'colorbar.tickfont.size'}
                    units="px"
                  />
                  <ColorPicker label={_('字体颜色')} attr={prefix + 'colorbar.tickfont.color'} />
                  <Dropdown
                    label={_('角度')}
                    attr={prefix + 'colorbar.tickangle'}
                    clearable={false}
                    options={[
                      {label: _('自动'), value: 'auto'},
                      {label: _('45'), value: 45},
                      {label: _('90'), value: 90},
                      {label: _('135'), value: 135},
                      {label: _('180'), value: 180},
                    ]}
                  />
                  <Dropdown
                    label={_('指数')}
                    attr={prefix + 'colorbar.exponentformat'}
                    clearable={false}
                    options={[
                      {label: _('空'), value: '000'},
                      {label: _('e+6'), value: 'e'},
                      {label: _('E+6'), value: 'E'},
                      {label: _('x10^6'), value: 'power'},
                      {label: _('k/M/G'), value: 'SI'},
                      {label: _('k/M/B'), value: 'B'},
                    ]}
                  />
                  <DropdownCustom
                    label={_('标签前缀')}
                    attr={prefix + 'colorbar.tickprefix'}
                    options={[
                      {label: _('空'), value: ''},
                      {label: _('x'), value: 'x'},
                      {label: _('$'), value: '$'},
                      {label: _('#'), value: '#'},
                      {label: _('@'), value: '@'},
                      {label: _('自定义'), value: 'custom'},
                    ]}
                    customOpt="custom"
                    dafaultOpt=""
                    clearable={false}
                  />
                  <Dropdown
                    label={_('显示前缀')}
                    attr={prefix + 'colorbar.showtickprefix'}
                    options={[
                      {label: _('任一标签'), value: 'all'},
                      {label: _('第一个标签'), value: 'first'},
                      {label: _('最后一个标签'), value: 'last'},
                      {label: _('空标签'), value: 'none'},
                    ]}
                  />

                  <DropdownCustom
                    label={_('标签后缀')}
                    attr={prefix + 'colorbar.ticksuffix'}
                    options={[
                      {label: _('空'), value: ''},
                      {label: _('C'), value: 'C'},
                      {label: _('%'), value: '%'},
                      {label: _('^'), value: '^'},
                      {label: _('自定义'), value: 'custom'},
                    ]}
                    customOpt="custom"
                    dafaultOpt=""
                    clearable={false}
                  />
                  <Dropdown
                    label={_('看显示后缀')}
                    attr={prefix + 'colorbar.showticksuffix'}
                    options={[
                      {label: _('任一标签'), value: 'all'},
                      {label: _('第一个标签'), value: 'first'},
                      {label: _('最后一个标签'), value: 'last'},
                      {label: _('空标签'), value: 'none'},
                    ]}
                  />
                  <Radio
                    attr={prefix + 'colorbar.tickmode'}
                    options={[
                      {label: _('自动'), value: 'auto'},
                      {label: _('自定义'), value: 'linear'},
                    ]}
                    label={_('打勾间隔')}
                  />

                  <Numeric label={_('步长偏移量')} attr={prefix + 'colorbar.tick0'} />
                  <Numeric label={_('步长大小')} attr={prefix + 'colorbar.dtick'} />
                  <Numeric label={_('最大标签数')} attr={prefix + 'colorbar.nticks'} />
                </VisibilitySelect>
              </PlotlyFold>
              <PlotlyFold name={_('打勾')}>
                <VisibilitySelect
                  attr={prefix + 'colorbar.ticks'}
                  options={[
                    {label: _('内部'), value: 'inside'},
                    {label: _('外部'), value: 'outside'},
                    {label: _('隐藏'), value: ''},
                  ]}
                  showOn={['inside', 'outside']}
                  defaultOpt={''}
                >
                  <Numeric label={_('长度')} attr={prefix + 'colorbar.ticklen'} units="px" />
                  <Numeric label={_('宽度')} attr={prefix + 'colorbar.tickwidth'} units="px" />
                  <ColorPicker label={_('颜色')} attr={prefix + 'colorbar.tickcolor'} />
                  <Radio
                    attr={prefix + 'colorbar.tickmode'}
                    options={[
                      {label: _('自动'), value: 'auto'},
                      {label: _('自定义'), value: 'linear'},
                    ]}
                    label={_('打勾间隔')}
                  />

                  <Numeric label={_('步长偏移量')} attr={prefix + 'colorbar.tick0'} />
                  <Numeric label={_('步长大小')} attr={prefix + 'colorbar.dtick'} />
                  <Numeric label={_('最大标签数')} attr={prefix + 'colorbar.nticks'} />
                </VisibilitySelect>
              </PlotlyFold>
              <PlotlyFold name={_('边界与背景')}>
                <PlotlySection name={_('条形图颜色')} attr={prefix + 'colorbar.outlinewidth'}>
                  <Numeric label={_('边界宽度')} attr={prefix + 'colorbar.outlinewidth'} />
                  <ColorPicker label={_('边界颜色')} attr={prefix + 'colorbar.outlinecolor'} />
                </PlotlySection>
                <PlotlySection name={_('彩条容器')} attr={prefix + 'colorbar.bgcolor'}>
                  <ColorPicker label={_('背景色')} attr={prefix + 'colorbar.bgcolor'} />
                  <Numeric label={_('边界宽度')} attr={prefix + 'colorbar.borderwidth'} />
                  <ColorPicker label={_('边界颜色')} attr={prefix + 'colorbar.bordercolor'} />
                </PlotlySection>
              </PlotlyFold>
            </PlotlyPanel>
          </VisibilitySelect>
        );
      })}
    </TraceAccordion>
  );
};

StyleColorBarsPanel.contextTypes = {
  localize: PropTypes.func,
};

export default StyleColorBarsPanel;
