import React from 'react';
import PropTypes from 'prop-types';
import {
  ColorPicker,
  FontSelector,
  Numeric,
  Radio,
  PlotlySection,
  Dropdown,
  SliderAccordion,
} from '../components';

const StyleSlidersPanel = (props, {localize: _}) => (
  <SliderAccordion>
    <Radio
      attr="visible"
      options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
    />
    <PlotlySection name={_('背景')}>
      <ColorPicker label={_('颜色')} attr="bgcolor" />
      <ColorPicker label={_('激活颜色')} attr="activebgcolor" />
    </PlotlySection>
    <PlotlySection name={_('边界')}>
      <Numeric label={_('宽度')} attr="borderwidth" />
      <ColorPicker label={_('颜色')} attr="bordercolor" />
    </PlotlySection>
    <PlotlySection name={_('字体')}>
      <FontSelector label={_('字体')} attr="font.family" />
      <Numeric label={_('大小')} attr="font.size" />
      <ColorPicker label={_('颜色')} attr="font.color" />
    </PlotlySection>
    <PlotlySection name={_('长度')} attr={'len'}>
      <Numeric label={_('长度')} attr={'len'} step={0.02} />
      <Dropdown
        label={_('长度模式')}
        attr={'lenmode'}
        options={[
          {label: _('画布分数'), value: 'fraction'},
          {label: _('像素'), value: 'pixels'},
        ]}
      />
    </PlotlySection>
    <PlotlySection name={_('水平定位')} attr={'x'}>
      <Numeric label={_('位置')} attr={'x'} showSlider step={0.02} />
      <Radio
        label={_('锚')}
        attr={'xanchor'}
        options={[
          {label: _('左'), value: 'left'},
          {label: _('居中'), value: 'center'},
          {label: _('右'), value: 'right'},
        ]}
      />
    </PlotlySection>
    <PlotlySection name={_('垂直定位')} attr={'y'}>
      <Numeric label={_('位置')} attr={'y'} showSlider step={0.02} />
      <Radio
        label={_('锚')}
        attr={'yanchor'}
        options={[
          {label: _('顶部'), value: 'top'},
          {label: _('中部'), value: 'middle'},
          {label: _('底部'), value: 'bottom'},
        ]}
      />
    </PlotlySection>
    <PlotlySection name={_('间隔')}>
      <Numeric label={_('底部')} attr="pad.t" units="px" />
      <Numeric label={_('底部')} attr="pad.b" units="px" />
      <Numeric label={_('左')} attr="pad.l" units="px" />
      <Numeric label={_('右')} attr="pad.r" units="px" />
    </PlotlySection>
    <PlotlySection name={_('打勾')}>
      <ColorPicker label={_('颜色')} attr="tickcolor" />
      <Numeric label={_('长度')} attr="ticklen" />
      <Numeric label={_('宽度')} attr="tickwidth" />
    </PlotlySection>
  </SliderAccordion>
);

StyleSlidersPanel.contextTypes = {
  localize: PropTypes.func,
};

export default StyleSlidersPanel;
