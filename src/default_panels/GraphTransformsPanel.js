import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  Radio,
  TransformAccordion,
  TraceAccordion,
  DataSelector,
  Dropdown,
  PlotlySection,
  FilterOperation,
  FilterValue,
} from '../components';
import {connectAggregationToTransform} from '../lib';
import {TRANSFORMABLE_TRACES} from 'lib/constants';

const AggregationSection = connectAggregationToTransform(PlotlySection);

export class Aggregations extends Component {
  render() {
    const {
      fullContainer: {aggregations = []},
    } = this.context;
    const {localize: _} = this.context;
    if (aggregations.length === 0) {
      return null;
    }

    return (
      <PlotlySection name={_('集合')} attr="aggregations">
        {aggregations
          .filter(aggr => aggr.target && aggr.target.match(/transforms\[\d*\]\./gi) === null)
          .map(({target}, i) => (
            <AggregationSection show key={i} aggregationIndex={i}>
              <Dropdown
                attr="func"
                label={target}
                options={[
                  {label: _('计数'), value: 'count'},
                  {label: _('求和'), value: 'sum'},
                  {label: _('平均值'), value: 'avg'},
                  {label: _('中值'), value: 'median'},
                  {label: _('模式'), value: 'mode'},
                  {label: _('均方根'), value: 'rms'},
                  {label: _('标准差'), value: 'stddev'},
                  {label: _('最小值'), value: 'min'},
                  {label: _('最大值'), value: 'max'},
                  {label: _('初始值'), value: 'first'},
                  {label: _('末尾置'), value: 'last'},
                  {label: _('Change'), value: 'change'},
                  {label: _('范围'), value: 'range'},
                ]}
                clearable={false}
              />
            </AggregationSection>
          ))}
      </PlotlySection>
    );
  }
}

Aggregations.plotly_editor_traits = {no_visibility_forcing: true};
Aggregations.contextTypes = {
  fullContainer: PropTypes.object,
  localize: PropTypes.func,
};

const GraphTransformsPanel = (props, {localize: _}) => {
  return (
    <TraceAccordion traceFilterCondition={t => TRANSFORMABLE_TRACES.includes(t.type)}>
      <TransformAccordion>
        <Radio
          attr="enabled"
          options={[{label: _('启动'), value: true}, {label: _('关闭'), value: false}]}
        />

        <DataSelector label={_('By')} attr="groups" />

        <DataSelector label={_('目标')} attr="target" />
        <FilterOperation label={_('运算')} attr="operation" />
        <FilterValue label={_('值')} attr="value" />

        <Radio
          attr="order"
          options={[
            {label: _('升序'), value: 'ascending'},
            {label: _('降序'), value: 'descending'},
          ]}
        />

        <Aggregations />
      </TransformAccordion>
    </TraceAccordion>
  );
};

GraphTransformsPanel.contextTypes = {
  localize: PropTypes.func,
};

export default GraphTransformsPanel;
