import React from 'react';
import PropTypes from 'prop-types';
import {
  ShapeAccordion,
  Radio,
  PlotlySection,
  PositioningRef,
  PositioningNumeric,
  Numeric,
  NumericFraction,
  ColorPicker,
  LineDashSelector,
} from '../components';

const StyleShapesPanel = (props, {localize: _}) => (
  <ShapeAccordion canAdd>
    <Radio
      attr="visible"
      options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
    />
    <Radio
      attr="type"
      options={[
        {label: _('线'), value: 'line'},
        {label: _('矩形'), value: 'rect'},
        {label: _('椭圆'), value: 'circle'},
      ]}
    />

    <PlotlySection name={_('水平边界')}>
      <PositioningRef label={_('Relative to')} attr="xref" />
      <PositioningNumeric label={_('初始点')} attr="x0" />
      <PositioningNumeric label={_('终点')} attr="x1" />
    </PlotlySection>

    <PlotlySection name={_('垂直边界')}>
      <PositioningRef label={_('Relative to')} attr="yref" />
      <PositioningNumeric label={_('初始点')} attr="y0" />
      <PositioningNumeric label={_('终点')} attr="y1" />
    </PlotlySection>
    <PlotlySection name={_('线')}>
      <Numeric label={_('宽度')} attr="line.width" />
      <ColorPicker label={_('颜色')} attr="line.color" />
      <LineDashSelector label={_('类型')} attr="line.dash" />
    </PlotlySection>
    <PlotlySection name={_('填充')}>
      <ColorPicker label={_('颜色')} attr="fillcolor" />
      <NumericFraction label={_('Opacity')} attr="opacity" />
    </PlotlySection>
  </ShapeAccordion>
);

StyleShapesPanel.contextTypes = {
  localize: PropTypes.func,
};

export default StyleShapesPanel;
