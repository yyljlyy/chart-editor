import React from 'react';
import PropTypes from 'prop-types';

import {
  Flaglist,
  ContourNumeric,
  LineDashSelector,
  LineShapeSelector,
  Numeric,
  NumericFraction,
  NumericFractionInverse,
  Radio,
  TextEditor,
  PlotlySection,
  LayoutSection,
  SymbolSelector,
  TraceAccordion,
  TraceTypeSection,
  TraceMarkerSection,
  ColorscalePicker,
  ColorwayPicker,
  HoverInfo,
  Dropdown,
  FillDropdown,
  FontSelector,
  TextPosition,
  MarkerSize,
  MarkerColor,
  MultiColorPicker,
  ErrorBars,
  DataSelector,
  VisibilitySelect,
  GroupCreator,
} from '../components';
import {
  BinningDropdown,
  NumericReciprocal,
  ShowInLegend,
  TextInfo,
  HoveronDropdown,
} from '../components/fields/derived';

const StyleTracesPanel = (props, {localize: _}) => (
  <TraceAccordion canGroup>
    <TextEditor label={_('名称')} attr="name" richTextOnly />
    <NumericFraction label={_('Trace Opacity')} attr="opacity" />
    <PlotlySection name={_('图例')}>
      <ShowInLegend
        label={_('在图例中显示')}
        attr="showlegend"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
        showOn={true}
      >
        <GroupCreator label={_('图例组')} prefix={_('Group')} attr="legendgroup" />
      </ShowInLegend>
    </PlotlySection>
    <PlotlySection name={_('Cones & Streamtubes')}>
      <Numeric label={_('大小')} attr="sizeref" stepmode="relative" />
      <Dropdown
        label={_('大小')}
        options={[{label: _('比例'), value: 'scaled'}, {label: _('绝对值'), value: 'absolute'}]}
        attr="sizemode"
      />
      <Dropdown
        label={_('Cone Anchor')}
        options={[
          {label: _('Tip'), value: 'tip'},
          {label: _('尾部'), value: 'tail'},
          {label: _('中心'), value: 'center'},
          {label: _('质心'), value: 'cm'},
        ]}
        attr="anchor"
      />
      <Numeric label={_('最大管道划分')} attr="maxdisplayed" />
    </PlotlySection>
    <MultiColorPicker label={_('颜色')} attr="color" />
    <TraceTypeSection name={_('饼图颜色')} traceTypes={['pie']} mode="trace">
      <LayoutSection attr="name">
        <ColorwayPicker label={_('颜色')} attr="piecolorway" />
        <Radio
          label={_('扩展色')}
          attr="extendpiecolors"
          options={[{label: _('打开'), value: true}, {label: _('关闭'), value: false}]}
        />
      </LayoutSection>
    </TraceTypeSection>
    <PlotlySection name={_('饼图标题')} attr="title">
      <TextEditor label={_('名称')} attr="title" />
      <Dropdown
        label={'标题位置'}
        attr="titleposition"
        options={[
          {label: _('左上角'), value: 'top left'},
          {label: _('上居中'), value: 'top center'},
          {label: _('右上角'), value: 'top right'},
          {label: _('居中'), value: 'middle center'},
          {label: _('左下角'), value: 'bottom left'},
          {label: _('下居中'), value: 'bottom center'},
          {label: _('右下角'), value: 'bottom right'},
        ]}
      />
      <FontSelector label={_('字体')} attr="titlefont.family" clearable={false} />
      <Numeric label={_('字体大小')} attr="titlefont.size" units="px" />
    </PlotlySection>
    <PlotlySection name={_('值')}>
      <BinningDropdown label={_('直方图函数')} attr="histfunc" />
      <Dropdown
        label={_('直方图归一化')}
        options={[
          {label: _('频数'), value: ''},
          {label: _('百分比'), value: 'percent'},
          {label: _('概率'), value: 'probability'},
          {label: _('密度'), value: 'density'},
          {label: _('概率密度'), value: 'probability density'},
        ]}
        attr="histnorm"
      />
    </PlotlySection>
    <PlotlySection name={_('累积')}>
      <Radio
        label={_('累积')}
        attr="cumulative.enabled"
        options={[{label: _('启用'), value: true}, {label: _('关闭'), value: false}]}
      />
      <Radio
        label={_('方向')}
        attr="cumulative.direction"
        options={[
          {label: _('递增'), value: 'increasing'},
          {label: _('递减'), value: 'decreasing'},
        ]}
      />
      <Radio
        label={_('当前绑定')}
        attr="cumulative.currentbin"
        options={[
          {label: _('包含'), value: 'include'},
          {label: _('不包含'), value: 'exclude'},
          {label: _('半包含'), value: 'half'},
        ]}
      />
    </PlotlySection>

    <PlotlySection name={_('标题')}>
      <Numeric label={_('高')} attr="header.height" />
      <MultiColorPicker label={_('填充颜色')} attr="header.fill.color" />
      <FontSelector label={_('字体')} attr="header.font.family" />
      <Numeric label={_('字体大小')} attr="header.font.size" />
      <Dropdown
        label={_('文本对齐')}
        options={[
          {label: _('左对齐'), value: 'left'},
          {label: _('居中'), value: 'center'},
          {label: _('右对齐'), value: 'right'},
        ]}
        attr="header.align"
      />
      <MultiColorPicker label={_('字体颜色')} attr="header.font.color" />
      <Numeric label={_('边框宽度')} attr="header.line.width" />
      <MultiColorPicker label={_('边框颜色')} attr="header.line.color" />
    </PlotlySection>
    <PlotlySection name={_('单元')}>
      <Numeric label={_('高')} attr="cells.height" />
      <MultiColorPicker label={_('填充颜色or')} attr="cells.fill.color" />
      <FontSelector label={_('字体')} attr="cells.font.family" />
      <Numeric label={_('字体大小')} attr="cells.font.size" />
      <Dropdown
        label={_('文本对齐')}
        options={[
          {label: _('左对齐'), value: 'left'},
          {label: _('居中'), value: 'center'},
          {label: _('右对齐'), value: 'right'},
        ]}
        attr="cells.align"
      />
      <MultiColorPicker label={_('字体颜色')} attr="cells.font.color" />
      <Numeric label={_('边框宽度')} attr="cells.line.width" />
      <MultiColorPicker label={_('边框颜色')} attr="cells.line.color" />
    </PlotlySection>
    <PlotlySection name={_('显示')}>
      <Flaglist
        attr="mode"
        options={[
          {label: _('点'), value: 'markers'},
          {label: _('线'), value: 'lines'},
          {label: _('文本'), value: 'text'},
        ]}
      />
      <Radio
        attr="flatshading"
        label={_('平铺')}
        options={[{label: _('启用'), value: true}, {label: _('关闭'), value: false}]}
      />
    </PlotlySection>
    <PlotlySection name={_('绑定')}>
      <Numeric label={_('X 绑定起始值')} attr="xbins.start" axis="x" />
      <Numeric label={_('X 绑定终止值')} attr="xbins.end" axis="x" />
      <Numeric label={_('X 绑定值大小')} attr="xbins.size" axis="x" />
      <Numeric label={_('最大X绑定值')} attr="nbinsx" />

      <Numeric label={_('Y 绑定起始值')} attr="ybins.start" axis="y" />
      <Numeric label={_('Y 绑定终止值')} attr="ybins.end" axis="y" />
      <Numeric label={_('Y 绑定值大小')} attr="ybins.size" axis="y" />
      <Numeric label={_('最大Y绑定值')} attr="nbinsy" />
    </PlotlySection>
    <PlotlySection name={_('条形位置')}>
      <Numeric label={_('基础值')} attr="base" />
      <Numeric label={_('偏移量')} attr="offset" />
      <Numeric label={_('宽度')} attr="width" />
    </PlotlySection>

    <TraceMarkerSection>
      <Radio
        label={_('顺序')}
        attr="sort"
        options={[{label: _('排序'), value: true}, {label: _('不排序'), value: false}]}
      />
      <Radio
        label={_('方向')}
        attr="direction"
        options={[
          {label: _('顺时针方向'), value: 'clockwise'},
          {label: _('逆时针方向'), value: 'counterclockwise'},
        ]}
      />
      <Numeric label={_('旋转')} attr="rotation" />
      <NumericFraction label={_('孔大小')} attr="hole" />
      <NumericFraction label={_('拉')} attr="pull" />
      <Dropdown
        options={[
          {label: _('全部显示'), value: 'all'},
          {label: _('离群点'), value: 'outliers'},
          {label: _('疑似离群点'), value: 'suspectedoutliers'},
          {label: _('隐藏'), value: false},
        ]}
        attr="boxpoints"
        clearable={false}
      />
      <Dropdown
        options={[
          {label: _('全部显示'), value: 'all'},
          {label: _('离群点'), value: 'outliers'},
          {label: _('疑似离群点'), value: 'suspectedoutliers'},
          {label: _('隐藏'), value: false},
        ]}
        attr="points"
        clearable={false}
      />
      <NumericFraction label={_('Jitter')} attr="jitter" />
      <Numeric label={_('位置')} attr="pointpos" step={0.1} showSlider />
      <MarkerColor suppressMultiValuedMessage label={_('Color')} attr="marker.color" />
      <NumericFraction label={_('模糊点')} attr="marker.opacity" />
      <MarkerSize label={_('大小')} attr="marker.size" />
      <NumericReciprocal
        label={_('大小比例')}
        attr="marker.sizeref"
        step={0.2}
        stepmode="relative"
      />
      <Radio
        label={_('大小模式')}
        attr="marker.sizemode"
        options={[{label: _('区域'), value: 'area'}, {label: _('直径'), value: 'diameter'}]}
      />
      <Numeric label={_('最小尺寸')} attr="marker.sizemin" />
      <SymbolSelector label={_('特征')} attr="marker.symbol" />
      <Numeric label={_('边框宽度')} attr="marker.line.width" />
      <MultiColorPicker label={_('边框颜色')} attr="marker.line.color" />
      <Numeric label={_('频数最大点')} attr="marker.maxdisplayed" />
    </TraceMarkerSection>

    <TraceTypeSection
      name={_('条形大小和面积')}
      traceTypes={['bar', 'histogram']}
      mode="trace"
    >
      <LayoutSection attr="name">
        <Dropdown
          label={_('条形图模式')}
          attr="barmode"
          options={[
            {label: _('覆盖'), value: 'overlay'},
            {label: _('组'), value: 'group'},
            {label: _('栈'), value: 'stack'},
            {label: _('相关性'), value: 'relative'},
          ]}
          clearable={false}
        />
        <Dropdown
          label={_('归一化')}
          attr="barnorm"
          options={[
            {label: _('空'), value: ''},
            {label: _('分数'), value: 'fraction'},
            {label: _('百分比'), value: 'percent'},
          ]}
          clearable={false}
        />
        <NumericFractionInverse label={_('条形宽度')} attr="bargap" />
        <NumericFraction label={_('条形组间距')} attr="bargroupgap" />
      </LayoutSection>
    </TraceTypeSection>
    <TraceTypeSection name={_('箱线图大小和面积')} traceTypes={['box']} mode="trace">
      <LayoutSection attr="name">
        <Radio
          label={_('箱线图模式')}
          attr="boxmode"
          options={[{label: _('覆盖'), value: 'overlay'}, {label: _('组'), value: 'group'}]}
        />
        <NumericFractionInverse label={_('箱线图宽度')} attr="boxgap" />
        <NumericFraction label={_('箱线图组间距')} attr="boxgroupgap" />
      </LayoutSection>
    </TraceTypeSection>
    <TraceTypeSection name={_('折叠栏大小和面积')} traceTypes={['violin']} mode="trace">
      <LayoutSection attr="name">
        <Radio
          label={_('折叠栏模式')}
          attr="violinmode"
          options={[{label: _('覆盖'), value: 'overlay'}, {label: _('组'), value: 'group'}]}
        />
        <NumericFractionInverse label={_('折叠栏宽度')} attr="violingap" />
        <NumericFraction label={_('折叠栏组间距')} attr="violingroupgap" />
      </LayoutSection>
    </TraceTypeSection>

    <NumericFraction label={_('内宽')} attr="whiskerwidth" />
    <PlotlySection name={_('标记')}>
      <Numeric label={_('宽度')} attr="tickwidth" />
    </PlotlySection>
    <PlotlySection name={_('等高线')}>
      <Radio
        label={_('类型')}
        attr="contours.type"
        options={[
          {label: _('水平'), value: 'levels'},
          {label: _('约束'), value: 'constraint'},
        ]}
      />
      <Dropdown
        label={_('色彩')}
        attr="contours.coloring"
        options={[
          {label: _('填充'), value: 'fill'},
          {label: _('热图'), value: 'heatmap'},
          {label: _('线'), value: 'lines'},
          {label: _('空'), value: 'none'},
        ]}
        clearable={false}
      />
      <Radio
        label={_('等高线')}
        attr="contours.showlines"
        options={[{label: _('打开'), value: true}, {label: _('关闭'), value: false}]}
      />
      <Radio
        label={_('等高线图标')}
        attr="contours.showlabels"
        options={[{label: _('打开'), value: true}, {label: _('关闭'), value: false}]}
      />
      <Radio
        label={_('等高线数量')}
        attr="autocontour"
        options={[{label: _('自动'), value: true}, {label: _('Custom'), value: false}]}
      />
      <Numeric label={_('最大等高线')} attr="ncontours" />

      <ContourNumeric label={_('步长')} attr="contours.size" />
      <ContourNumeric label={_('最小等高线')} attr="contours.start" />
      <ContourNumeric label={_('最大等高线')} attr="contours.end" />
    </PlotlySection>
    <TraceTypeSection name={_('堆叠')} traceTypes={['scatter']} mode="trace">
      <GroupCreator label={_('组')} prefix={_('Stack')} attr="stackgroup" />
      <Radio
        label={_('间隔')}
        attr="stackgaps"
        options={[
          {label: _('Infer Zero'), value: 'infer zero'},
          {label: _('插入'), value: 'interpolate'},
        ]}
      />
      <Radio
        label={_('方向')}
        attr="orientation"
        options={[{label: _('水平'), value: 'h'}, {label: _('垂直'), value: 'v'}]}
      />
      <Radio
        label={_('归一化')}
        attr="groupnorm"
        options={[
          {label: _('空'), value: ''},
          {label: _('分数'), value: 'fraction'},
          {label: _('百分比'), value: 'percent'},
        ]}
      />
    </TraceTypeSection>
    <TraceTypeSection
      name={_('线')}
      traceTypes={[
        'scatter',
        'contour',
        'scatterternary',
        'scatterpolar',
        'scatterpolargl',
        'scatter3d',
        'scattergl',
        'scattergeo',
        'scattermapbox',
        'box',
        'violin',
      ]}
      mode="trace"
    >
      <Numeric label={_('宽')} attr="line.width" />
      <MultiColorPicker label={_('颜色')} attr="line.color" />
      <Radio
        label={_('颜色条')}
        attr="line.showscale"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
      <ColorscalePicker label={_('色标')} attr="line.colorscale" />
      <LineDashSelector label={_('类型')} attr="line.dash" />
      <LineShapeSelector label={_('形状')} attr="line.shape" />
      <Numeric label={_('光滑')} attr="line.smoothing" showSlider step={0.1} />
      <Radio
        label={_('连接间隔')}
        attr="connectgaps"
        options={[{label: _('连接'), value: true}, {label: _('空白'), value: false}]}
      />
    </TraceTypeSection>
    <PlotlySection name={_('填充面积')}>
      <FillDropdown attr="fill" label={_('填充到')} />
      <MultiColorPicker label={_('颜色')} attr="fillcolor" />
    </PlotlySection>
    <PlotlySection name={_('文本属性')}>
      <TextInfo attr="textinfo" />
    </PlotlySection>
    <TraceTypeSection
      name={_('文本')}
      traceTypes={[
        'scatter',
        'scattergl',
        'scatterpolar',
        'scatterpolargl',
        'barpolar',
        'pie',
        'scatter3d',
        'scatterternary',
        'bar',
        'scattergeo',
        'scattermapbox',
      ]}
      mode="trace"
    >
      <DataSelector label={_('文本')} attr="text" />
      <TextPosition label={_('文本位置')} attr="textposition" />
      <FontSelector label={_('字体')} attr="textfont.family" />
      <Numeric label={_('字体大小')} attr="textfont.size" units="px" />
      <MultiColorPicker label={_('字体颜色')} attr="textfont.color" />
    </TraceTypeSection>
    <PlotlySection name={_('色标')}>
      <ColorscalePicker label={_('色标')} attr="colorscale" />
      <Radio
        label={_('颜色条')}
        attr="showscale"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
      <Radio
        label={_('方向')}
        attr="reversescale"
        options={[{label: _('正常'), value: false}, {label: _('颠倒'), value: true}]}
      />
      <VisibilitySelect
        label={_('Range')}
        attr="zauto"
        options={[{label: _('自动'), value: true}, {label: _('自定义'), value: false}]}
        showOn={false}
        defaultOpt={true}
      >
        <Numeric label={_('最小值')} attr="zmin" />
        <Numeric label={_('最大值')} attr="zmax" />
      </VisibilitySelect>
      <VisibilitySelect
        label={_('范围')}
        attr="cauto"
        options={[{label: _('自动'), value: true}, {label: _('自定义'), value: false}]}
        showOn={false}
        defaultOpt={true}
      >
        <Numeric label={_('最小值')} attr="cmin" />
        <Numeric label={_('最大值')} attr="cmax" />
      </VisibilitySelect>
      <Radio
        label={_('光滑')}
        attr="zsmooth"
        options={[{label: _('打开'), value: 'best'}, {label: _('关闭'), value: false}]}
      />
    </PlotlySection>
    <PlotlySection name={_('单元间隔')}>
      <Numeric label={_('水平间隔')} attr="xgap" />
      <Numeric label={_('垂直间隔')} attr="ygap" />
    </PlotlySection>
    <PlotlySection name={_('热图')}>
      <Numeric label={_('水平间隔')} attr="xgap" />
      <Numeric label={_('垂直间隔')} attr="ygap" />
    </PlotlySection>
    <TraceTypeSection
      name={_('数据间隔')}
      traceTypes={['heatmap', 'contour', 'heatmapgl']}
      mode="trace"
    >
      <Radio
        label={_('插入间隔')}
        attr="connectgaps"
        options={[{label: _('打开'), value: true}, {label: _('关闭'), value: false}]}
      />
    </TraceTypeSection>
    <PlotlySection name={_('亮度')}>
      <NumericFraction label={_('Ambient')} attr="lighting.ambient" />
      <NumericFraction label={_('Diffuse')} attr="lighting.diffuse" />
      <NumericFraction label={_('Specular')} attr="lighting.specular" />
      <NumericFraction label={_('Roughness')} attr="lighting.roughness" />
      <NumericFraction label={_('Fresnel')} attr="lighting.fresnel" />
      <NumericFraction label={_('Vertex Normal')} attr="lighting.vertexnormalsepsilon" />
      <NumericFraction label={_('Face Normal')} attr="lighting.facenormalsepsilon" />
    </PlotlySection>
    <PlotlySection name={_('亮度位置')}>
      <NumericFraction label={_('X')} attr="lightposition.x" />
      <NumericFraction label={_('Y')} attr="lightposition.y" />
      <NumericFraction label={_('Z')} attr="lightposition.z" />
    </PlotlySection>
    <PlotlySection name={_('增长轨迹类型')}>
      <TextEditor label={_('名称')} attr="increasing.name" richTextOnly />
      <Numeric label={_('宽度')} attr="increasing.line.width" />
      <MultiColorPicker label={_('线条颜色')} attr="increasing.line.color" />
      <MultiColorPicker label={_('填充颜色')} attr="increasing.fillcolor" />
      <LineDashSelector label={_('类型')} attr="increasing.line.dash" />
      <Radio
        label={_('在图例中显示')}
        attr="increasing.showlegend"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
    </PlotlySection>
    <PlotlySection name={_('降低轨迹类型')}>
      <TextEditor label={_('名称')} attr="decreasing.name" richTextOnly />
      <Numeric label={_('宽度')} attr="decreasing.line.width" />
      <MultiColorPicker label={_('线条颜色')} attr="decreasing.line.color" />
      <MultiColorPicker label={_('填充颜色')} attr="decreasing.fillcolor" />
      <LineDashSelector label={_('类型')} attr="decreasing.line.dash" />
      <Radio
        label={_('在图例中显示')}
        attr="decreasing.showlegend"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
    </PlotlySection>
    <PlotlySection name={_('缩放比例')}>
      <GroupCreator label={_('缩放组')} prefix={_('Group')} attr="scalegroup" />
      <Radio
        label={_('缩放模式')}
        attr="scalemode"
        options={[{label: _('宽度'), value: 'width'}, {label: _('计数'), value: 'count'}]}
      />
      <Radio
        label={_('分屏模式')}
        attr="spanmode"
        options={[
          {label: _('软件'), value: 'soft'},
          {label: _('硬件'), value: 'hard'},
          {label: _('指南'), value: 'manual'},
        ]}
      />
      <Numeric label={_('带宽')} attr="bandwidth" />
      <Numeric label={_('分屏')} attr="span" />
      <Radio
        attr="side"
        label={_('可见侧')}
        options={[
          {label: _('全部'), value: 'both'},
          {label: _('正向'), value: 'positive'},
          {label: _('负向'), value: 'negative'},
        ]}
      />
    </PlotlySection>
    <PlotlySection name={_('箱型均值')}>
      <Radio
        attr="boxmean"
        options={[
          {label: _('均值'), value: true},
          {label: _('均值 & 标准差'), value: 'sd'},
          {label: _('空'), value: false},
        ]}
      />
    </PlotlySection>
    <PlotlySection name={_('箱线图')}>
      <Radio
        attr="box.visible"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
      <NumericFraction label={_('箱线宽度')} attr="box.width" />
      <MultiColorPicker label={_('箱线图填充颜色')} attr="box.color" />
      <NumericFraction label={_('箱线图线宽度')} attr="box.line.width" />
      <MultiColorPicker label={_('箱线图线颜色')} attr="box.line.color" />
    </PlotlySection>
    <PlotlySection name={_('中线')}>
      <Radio
        attr="meanline.visible"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
      <NumericFraction label={_('中线宽度')} attr="meanline.width" />
      <MultiColorPicker label={_('中线颜色')} attr="meanline.color" />
    </PlotlySection>
    <PlotlySection name={_('关于停')}>
      <HoverInfo attr="hoverinfo" label={_('悬停时显示值')} />
      <Radio
        label={_('拆分标签')}
        attr="hoverlabel.split"
        options={[{label: _('是'), value: true}, {label: _('否'), value: false}]}
      />
      <VisibilitySelect
        attr="contour.show"
        label={_('显示等高线')}
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
        showOn={true}
        defaultOpt={false}
      >
        <MultiColorPicker label={_('等高线颜色')} attr="contour.color" />
        <Numeric label={_('等高线宽度')} attr="contour.width" />
      </VisibilitySelect>
    </PlotlySection>

    <PlotlySection name={_('悬停动作')}>
      <HoveronDropdown attr="hoveron" label={_('悬停')} />
    </PlotlySection>

    <TraceTypeSection
      name={_('误差条X')}
      traceTypes={['scatter', 'scattergl', 'scatter3d', 'bar']}
      mode="trace"
    >
      <ErrorBars attr="error_x" />
    </TraceTypeSection>

    <TraceTypeSection
      name={_('误差条Y')}
      traceTypes={['scatter', 'scattergl', 'scatter3d', 'bar']}
      mode="trace"
    >
      <ErrorBars attr="error_y" />
    </TraceTypeSection>

    <TraceTypeSection name={_('误差条Z')} traceTypes={['scatter3d']} mode="trace">
      <ErrorBars attr="error_z" />
    </TraceTypeSection>
  </TraceAccordion>
);

StyleTracesPanel.contextTypes = {
  localize: PropTypes.func,
};

export default StyleTracesPanel;
