import React from 'react';
import PropTypes from 'prop-types';
import {
  SubplotAccordion,
  RectanglePositioner,
  AxisOverlayDropdown,
  PlotlySection,
  TraceTypeSection,
  AxisAnchorDropdown,
  AxisSide,
  Dropdown,
  Radio,
  Numeric,
  ColorPicker,
  VisibilitySelect,
  NumericFraction,
} from '../components';
import {TRACE_TO_AXIS} from '../lib/constants';

const GraphSubplotsPanel = (props, {localize: _}) => (
  <SubplotAccordion>
    <PlotlySection name={_('边界')} attr="xaxis.domain[0]">
      <AxisOverlayDropdown label={_('X覆盖')} attr="xaxis.overlaying" />
      <AxisOverlayDropdown label={_('Y覆盖')} attr="yaxis.overlaying" />
    </PlotlySection>

    <RectanglePositioner attr="domain.x[0]" />
    <RectanglePositioner attr="xaxis.domain[0]" cartesian />

    <TraceTypeSection name={_('X Anchor')} traceTypes={TRACE_TO_AXIS.cartesian}>
      <AxisAnchorDropdown label={_('Anchor to')} attr="xaxis.anchor" clearable={false} />
      <AxisSide label={_('侧')} attr="xaxis.side" />
    </TraceTypeSection>
    <TraceTypeSection name={_('Y Anchor')} traceTypes={TRACE_TO_AXIS.cartesian}>
      <AxisAnchorDropdown label={_('Anchor to')} attr="yaxis.anchor" clearable={false} />
      <AxisSide label={_('侧')} attr="yaxis.side" />
    </TraceTypeSection>

    <PlotlySection name={_('宽高比')}>
      <VisibilitySelect
        attr="aspectmode"
        options={[
          {label: _('自动'), value: 'mode'},
          {label: _('立方体'), value: 'cube'},
          {label: _('数据'), value: 'data'},
          {label: _('指南'), value: 'manual'},
        ]}
        dropdown={true}
        clearable={false}
        showOn="manual"
        defaultOpt="mode"
      >
        <Numeric label={_('X')} attr="aspectratio.x" step={0.1} />
        <Numeric label={_('Y')} attr="aspectratio.y" step={0.1} />
        <Numeric label={_('Z')} attr="aspectratio.z" step={0.1} />
      </VisibilitySelect>
    </PlotlySection>

    <PlotlySection name={_('画布')}>
      <ColorPicker label={_('画图背景')} attr="bgcolor" />
    </PlotlySection>

    <PlotlySection name={_('条形图属性')}>
      <Radio
        label={_('条形图模式')}
        attr="barmode"
        options={[{label: _('栈'), value: 'stack'}, {label: _('覆盖'), value: 'overlay'}]}
      />
      <NumericFraction label={_('条形间隔')} attr="bargap" showSlider />
    </PlotlySection>

    <PlotlySection name={_('地图类型')}>
      <Dropdown
        label={_('Mapbox Style')}
        attr="style"
        options={[
          {label: _('基础'), value: 'basic'},
          {label: _('户外'), value: 'outdoors'},
          {label: _('亮'), value: 'light'},
          {label: _('暗'), value: 'dark'},
          {label: _('卫星'), value: 'satellite'},
          {label: _('全景图'), value: 'satellite-streets'},
        ]}
        clearable={false}
      />
    </PlotlySection>
    <PlotlySection name={_('地图定位')}>
      <Numeric label={_('中心纬度')} attr="center.lat" />
      <Numeric label={_('中心经度')} attr="center.lon" />
      <Numeric label={_('缩放比例')} attr="zoom" min={0} />
      <Numeric label={_('轴承')} attr="bearing" />
      <Numeric label={_('最高点')} attr="pitch" min={0} />
    </PlotlySection>

    <PlotlySection name={_('地图投影')}>
      <Dropdown
        label={_('区域')}
        attr="scope"
        options={[
          {label: _('世界'), value: 'world'},
          {label: _('美国'), value: 'usa'},
          {label: _('欧洲'), value: 'europe'},
          {label: _('亚洲'), value: 'asia'},
          {label: _('非洲'), value: 'africa'},
          {label: _('北美洲'), value: 'north america'},
          {label: _('南美洲'), value: 'south america'},
        ]}
        clearable={false}
      />
      <Dropdown
        label={_('投影')}
        attr="projection.type"
        clearable={false}
        options={[
          {label: _('等矩形'), value: 'equirectangular'},
          {label: _('Mercator'), value: 'mercator'},
          {label: _('Orthographic'), value: 'orthographic'},
          {label: _('Natural Earth'), value: 'natural earth'},
          {label: _('Albers USA'), value: 'albers usa'},
          {label: _('Winkel Tripel'), value: 'winkel tripel'},
          {label: _('Robinson'), value: 'robinson'},
          {label: _('Miller'), value: 'miller'},
          {label: _('Kavrayskiy 7'), value: 'kavrayskiy7'},
          {label: _('Eckert 4'), value: 'eckert4'},
          {label: _('等积斜方位投影'), value: 'azimuthal equal area'},
          {
            label: _('等距方位'),
            value: 'azimuthal equidistant',
          },
          {label: _('等面积圆锥曲线'), value: 'conic equal area'},
          {label: _('保形二次曲线'), value: 'conic conformal'},
          {label: _('等距圆锥曲线'), value: 'conic equidistant'},
          {label: _('Gnomonic'), value: 'gnomonic'},
          {label: _('Stereographic'), value: 'stereographic'},
          {label: _('Mollweide'), value: 'mollweide'},
          {label: _('Hammer'), value: 'hammer'},
          {label: _('Transverse Mercator'), value: 'transverse mercator'},
          {label: _('Aitoff'), value: 'aitoff'},
          {label: _('Sinusoidal'), value: 'sinusoidal'},
        ]}
      />
    </PlotlySection>

    <PlotlySection name={_('国界')} attr="showcountries">
      <Radio
        attr="showcountries"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
      <Numeric label={_('界线宽度')} attr="countrywidth" units="px" />
      <ColorPicker label={_('界线颜色')} attr="countrycolor" />
    </PlotlySection>
    <PlotlySection name={_('Sub-Country Unit Borders')} attr="showsubunits">
      <Radio
        attr="showsubunits"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
      <Numeric label={_('界线宽度')} attr="subunitwidth" units="px" />
      <ColorPicker label={_('界线颜色')} attr="subunitcolor" />
    </PlotlySection>
    <PlotlySection name={_('海岸线')} attr="showcoastlines">
      <Radio
        attr="showcoastlines"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
      <Numeric label={_('宽度')} attr="coastlinewidth" units="px" />
      <ColorPicker label={_('颜色')} attr="coastlinecolor" />
    </PlotlySection>
    <PlotlySection name={_('海洋')} attr="showocean">
      <Radio
        attr="showocean"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
      <ColorPicker label={_('颜色')} attr="oceancolor" />
    </PlotlySection>
    <PlotlySection name={_('陆地')} attr="showland">
      <Radio
        attr="showland"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
      <ColorPicker label={_('颜色')} attr="landcolor" />
    </PlotlySection>
    <PlotlySection name={_('湖')} attr="showlakes">
      <Radio
        attr="showlakes"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
      <ColorPicker label={_('颜色')} attr="lakecolor" />
    </PlotlySection>
    <PlotlySection name={_('河水')} attr="showrivers">
      <Radio
        attr="showrivers"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
      <Numeric label={_('宽度')} attr="riverwidth" units="px" />
      <ColorPicker label={_('颜色')} attr="rivercolor" />
    </PlotlySection>

    <PlotlySection name={_('地图框架')} attr="showframe">
      <Radio
        attr="showframe"
        options={[{label: _('显示'), value: true}, {label: _('隐藏'), value: false}]}
      />
      <Numeric label={_('宽度')} attr="framewidth" units="px" />
      <ColorPicker label={_('颜色')} attr="framecolor" />
    </PlotlySection>

    <PlotlySection name={_('地图属性s')}>
      <Radio
        label={_('分辨率')}
        attr="resolution"
        options={[{label: _('1:110,000,000'), value: 110}, {label: _('1:50,000,000'), value: 50}]}
      />
      <Numeric label={_('规模')} attr="projection.scale" min={0} />
      <Numeric label={_('纬度')} attr="projection.rotation.lon" min={0} />
      <Numeric label={_('经度')} attr="projection.rotation.lat" min={0} />
      <Numeric label={_('Roll')} attr="projection.rotation.roll" min={0} />
    </PlotlySection>

    <PlotlySection name={_('三元')}>
      <Numeric label={_('求和')} attr="sum" />
    </PlotlySection>

    <PlotlySection name={_('极扇区')}>
      <Numeric label={_('最小值')} attr="sector[0]" min={-360} max={360} showSlider />
      <Numeric label={_('最大值')} attr="sector[1]" min={-360} max={360} showSlider />
      <NumericFraction label={_('Hole')} attr="hole" min={0} max={100} showSlider />
    </PlotlySection>
  </SubplotAccordion>
);

GraphSubplotsPanel.contextTypes = {
  localize: PropTypes.func,
};

export default GraphSubplotsPanel;
