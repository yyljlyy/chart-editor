import React from 'react';
import PropTypes from 'prop-types';
import {
  DataSelector,
  Dropdown,
  Radio,
  PlotlySection,
  AxesCreator,
  SubplotCreator,
  TraceAccordion,
  TraceSelector,
  TraceTypeSection,
  LocationSelector,
} from '../components';
import {
  HistogramInfoVertical,
  HistogramInfoHorizontal,
  Histogram2d,
} from '../components/fields/derived';

const GraphCreatePanel = (props, {localize: _, setPanel}) => {
  return (
    <TraceAccordion
      canAdd
      traceFilterCondition={t =>
        !(t.transforms && t.transforms.some(tr => ['fit', 'moving-average'].includes(tr.type)))
      }
    >
      <TraceSelector label={_('图表类型')} attr="type" show />

      <LocationSelector attr="type" />

      <DataSelector label={_('值')} attr="values" />
      <DataSelector label={_('标签')} attr="labels" />

      <DataSelector
        label={{
          histogram2d: _('X 值'),
          histogram: _('X 值'),
          '*': _('X'),
        }}
        attr="x"
      />
      <DataSelector
        label={{
          histogram2d: _('Y 值'),
          histogram: _('Y 值'),
          '*': _('Y'),
        }}
        attr="y"
      />
      <DataSelector
        label={{
          choropleth: _('值'),
          histogram2d: _('Z 值'),
          '*': _('Z'),
        }}
        attr="z"
      />
      <Radio
        label={_('方向')}
        attr="orientation"
        options={[{label: _('垂直'), value: 'v'}, {label: _('水平'), value: 'h'}]}
      />
      <HistogramInfoVertical>
        {_(
          '注：在垂直方向上，X值会被绑定。如果有Y值，则其为对应直方图函数的Y轴值，可以在 '
        )}
        <a onClick={() => setPanel('Style', 'Traces')}>{_('Traces')}</a>
        {_(
          '控制面板中配置。如果Y值被忽略，则对应直方图函数的Y轴默认为X值的计数。'
        )}
      </HistogramInfoVertical>
      <HistogramInfoHorizontal>
        {_(
          '注：在水平方向上，Y值会被绑定。如果有X值，则其为对应直方图函数的X轴值，可以在 '
        )}
        <a onClick={() => setPanel('Style', 'Traces')}>{_('Traces')}</a>
        {_(
          '控制面板中配置。如果X值被忽略，则对应直方图函数的X轴默认为Y值的计数。'
        )}
      </HistogramInfoHorizontal>
      <Histogram2d>
        {_(
          '注：当X与Y值被绑定。如果有Z值，则其为对应直方图函数的Z轴值，可以在  '
        )}
        <a onClick={() => setPanel('Style', 'Traces')}>{_('Traces')}</a>
        {_(
          '控制面板中配置。如果Z值被忽略，则对应直方图函数的Z轴默认为X和Y值的计数。'
        )}
      </Histogram2d>
      <DataSelector label={_('I (属性)')} attr="i" />
      <DataSelector label={_('J (属性)')} attr="j" />
      <DataSelector label={_('K (属性)')} attr="k" />
      <DataSelector label={_('打开')} attr="open" />
      <DataSelector label={_('最大值')} attr="high" />
      <DataSelector label={_('最小值')} attr="low" />
      <DataSelector label={_('关闭 ')} attr="close" />
      <DataSelector label={_('A')} attr="a" />
      <DataSelector label={_('B')} attr="b" />
      <DataSelector label={_('C')} attr="c" />
      <DataSelector label={_('U')} attr="u" />
      <DataSelector label={_('V')} attr="v" />
      <DataSelector label={_('W')} attr="w" />
      <DataSelector label={_('X 起始')} attr="starts.x" />
      <DataSelector label={_('Y 起始')} attr="starts.y" />
      <DataSelector label={_('Z 起始')} attr="starts.z" />
      <DataSelector label={_('标题')} attr="header.values" />
      <DataSelector label={_('列')} attr="cells.values" />

      <TraceTypeSection traceTypes={['scatterpolar', 'scatterpolargl', 'barpolar']} mode="trace">
        <DataSelector label={_('半径')} attr="r" />
        <DataSelector label={_('θ')} attr="theta" />
        <Dropdown
          label={_('θ单位')}
          options={[
            {label: _('弧度'), value: 'radians'},
            {label: _('自由度'), value: 'degrees'},
            {label: _('梯度'), value: 'gradians'},
          ]}
          attr="thetaunit"
          clearable={false}
        />
      </TraceTypeSection>

      <AxesCreator attr="fake_attr" />
      <SubplotCreator attr="fake_attr" />

      <PlotlySection name={_('标题属性')}>
        <DataSelector label={_('填充颜色')} attr="header.fill.color" />
        <DataSelector label={_('字体颜色')} attr="header.font.color" />
        <DataSelector label={_('字体大小')} attr="header.font.size" />
      </PlotlySection>

      <PlotlySection name={_('单元属性')}>
        <DataSelector label={_('填充颜色')} attr="cells.fill.color" />
        <DataSelector label={_('字体颜色')} attr="cells.font.color" />
        <DataSelector label={_('字体大小')} attr="cells.font.size" />
      </PlotlySection>

      <PlotlySection name={_('列属性')}>
        <DataSelector label={_('宽')} attr="columnwidth" />
        <DataSelector label={_('顺序')} attr="columnorder" />
      </PlotlySection>

      <PlotlySection name={_('属性')}>
        <DataSelector label={_('强度')} attr="intensity" />
        <DataSelector label={_('面板颜色')} attr="facecolor" />
        <DataSelector label={_('顶点颜色')} attr="vertexcolor" />
        <Radio
          label={_('转置')}
          attr="transpose"
          options={[{label: _('否'), value: false}, {label: _('是'), value: true}]}
        />
      </PlotlySection>
    </TraceAccordion>
  );
};

export default GraphCreatePanel;
GraphCreatePanel.contextTypes = {
  localize: PropTypes.func,
  setPanel: PropTypes.func,
};
