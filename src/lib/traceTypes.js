/**
 * Trace type constants
 */

export const chartCategory = _ => {
  return {
    SIMPLE: {
      value: 'SIMPLE',
      label: _('简单'),
    },
    CHARTS_3D: {
      value: 'CHARTS_3D',
      label: _('3D图表'),
    },
    FINANCIAL: {
      value: 'FINANCIAL',
      label: _('金融'),
    },
    DISTRIBUTIONS: {
      value: 'DISTRIBUTIONS',
      label: _('分布'),
    },
    MAPS: {
      value: 'MAPS',
      label: _('地图'),
    },
    SPECIALIZED: {
      value: 'SPECIALIZED',
      label: _('专业'),
    },
    THREE_D: {
      value: '3D',
      label: _('3D'),
      maxColumns: 1,
    },
  };
};

// Layout specification for TraceTypeSelector.js
export const categoryLayout = _ => [
  chartCategory(_).SIMPLE,
  chartCategory(_).DISTRIBUTIONS,
  chartCategory(_).THREE_D,
  chartCategory(_).MAPS,
  chartCategory(_).FINANCIAL,
  chartCategory(_).SPECIALIZED,
];

export const traceTypes = _ => [
  {
    value: 'scatter',
    label: _('散点图'),
    category: chartCategory(_).SIMPLE,
  },
  {
    value: 'bar',
    label: _('条形图'),
    category: chartCategory(_).SIMPLE,
  },
  {
    value: 'line',
    label: _('折线图'),
    category: chartCategory(_).SIMPLE,
  },
  {
    value: 'area',
    label: _('面积图'),
    category: chartCategory(_).SIMPLE,
  },
  {
    value: 'heatmap',
    label: _('热力图'),
    category: chartCategory(_).SIMPLE,
  },
  {
    value: 'table',
    label: _('表格'),
    category: chartCategory(_).SIMPLE,
  },
  {
    value: 'contour',
    label: _('等高线'),
    category: chartCategory(_).SIMPLE,
  },
  {
    value: 'pie',
    label: _('饼图'),
    category: chartCategory(_).SIMPLE,
  },
  {
    value: 'scatter3d',
    label: _('3D 散点图'),
    category: chartCategory(_).THREE_D,
  },
  {
    value: 'line3d',
    label: _('3D 折线图'),
    category: chartCategory(_).THREE_D,
  },
  {
    value: 'surface',
    label: _('3D 曲面'),
    category: chartCategory(_).THREE_D,
  },
  {
    value: 'mesh3d',
    label: _('3D 网状图'),
    category: chartCategory(_).THREE_D,
  },
  {
    value: 'cone',
    label: _('圆锥体'),
    category: chartCategory(_).THREE_D,
  },
  {
    value: 'streamtube',
    label: _('流体图'),
    category: chartCategory(_).THREE_D,
  },
  {
    value: 'box',
    label: _('盒形图'),
    category: chartCategory(_).DISTRIBUTIONS,
  },
  {
    value: 'violin',
    label: _('小提琴图'),
    category: chartCategory(_).DISTRIBUTIONS,
  },
  {
    value: 'histogram',
    label: _('直方图'),
    category: chartCategory(_).DISTRIBUTIONS,
  },
  {
    value: 'histogram2d',
    label: _('2D 直方图'),
    category: chartCategory(_).DISTRIBUTIONS,
  },
  {
    value: 'histogram2dcontour',
    label: _('2D 等高线直方图'),
    category: chartCategory(_).DISTRIBUTIONS,
  },
  {
    value: 'choropleth',
    label: _('热点地图'),
    category: chartCategory(_).MAPS,
  },
  {
    value: 'scattermapbox',
    label: _('卫星地图'),
    category: chartCategory(_).MAPS,
  },
  {
    value: 'scattergeo',
    label: _('区域地图'),
    category: chartCategory(_).MAPS,
  },
  // {
  //   value: 'parcoords',
  //   label: _('Parallel Coordinates'),
  //   category: chartCategory(_).SPECIALIZED,
  // },
  // {
  //   value: 'sankey',
  //   label: _('Sankey'),
  //   category: chartCategory(_).SPECIALIZED,
  // },
  // {
  //   value: 'carpet',
  //   label: _('Carpet'),
  //   category: chartCategory(_).SPECIALIZED,
  // },
  {
    value: 'scatterpolar',
    label: _('雷达图'),
    category: chartCategory(_).SPECIALIZED,
  },
  {
    value: 'barpolar',
    label: _('玫瑰图'),
    category: chartCategory(_).SPECIALIZED,
  },
  {
    value: 'scatterternary',
    label: _('三元散点图'),
    category: chartCategory(_).SPECIALIZED,
  },
  {
    value: 'candlestick',
    label: _('烛形图'),
    category: chartCategory(_).FINANCIAL,
  },
  {
    value: 'ohlc',
    label: _('OHLC'),
    category: chartCategory(_).FINANCIAL,
  },
  // {
  //   value: 'pointcloud',
  //   label: _('Point Cloud'),
  //   category: chartCategory(_).THREE_D,
  // },
  {
    value: 'scattergl',
    icon: 'scatter',
    label: _('散点图'),
    category: chartCategory(_).THREE_D,
  },
  {
    value: 'scatterpolargl',
    icon: 'scatterpolar',
    label: _('玫瑰图'),
    category: chartCategory(_).THREE_D,
  },
  // {
  //   value: 'heatmapgl',
  //   icon: 'heatmap',
  //   label: _('Heatmap GL'),
  //   category: chartCategory(_).THREE_D,
  // },
];
